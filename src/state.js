import { reactive } from "vue";

const state = reactive({
    user: null,
    logged_in: false,
    endpoint: "your google apps script endpoint here"
});

export default state;